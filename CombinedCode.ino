//IMU, Magnetorquer and Reaction Wheel
//Written by Sundeep Sarwana, Allan Goodman, Dominic Chan, Grace Liu
//Last Modified 18 July 2020
//Library - PID by Brett Beauregard

#include <Arduino.h>
#include "CombinedCode.h"
#include "MPU9250.h"
#include "PID_v1.h"
#define MT_ON_TIME 100 //length of MT pulse (ms)
#define DEADZONE 0

MPU9250 mpu;

double Setpoint;
double magKp = 5.0;
double magKi = 0.01;
double magKd = 1;
double wheelKp = 5;
double wheelKi = 0.01;
double wheelKd = 1.0;


double wheelRoll;
double wheelPitch;
double wheelYaw;

double magRoll;
double magPitch;
double magYaw;

int takeAvgRoll[] = {0,0,0,0,0};
int takeAvgPitch[] = {0,0,0,0,0};
int takeAvgYaw[] = {0,0,0,0,0};
int avg_roll;
int avg_yaw;
int avg_pitch;
int prev_avg_roll = 0;
int prev_avg_yaw = 0;
int prev_avg_pitch = 0;
int prev_ms;
int prev_roll_vel = 0;
int prev_pitch_vel = 0;
int prev_yaw_vel = 0;
double roll_vel;
double pitch_vel;
double yaw_vel;

//PIDS
PID wheelRollPID(&roll_vel, &wheelRoll, &Setpoint, wheelKp, wheelKi, wheelKd, DIRECT);
PID wheelPitchPID(&pitch_vel, &wheelPitch, &Setpoint, wheelKp, wheelKi, wheelKd, DIRECT);
PID wheelYawPID(&yaw_vel, &wheelYaw, &Setpoint, wheelKp, wheelKi, wheelKd, DIRECT);

PID magRollPID(&roll_vel, &magRoll, &Setpoint, magKp, magKi, magKd, DIRECT);
PID magPitchPID(&pitch_vel, &magPitch, &Setpoint, magKp, magKi, magKd, DIRECT);
PID magYawPID(&yaw_vel, &magYaw, &Setpoint, magKp, magKi, magKd, DIRECT);


void setup()
{
    // Magnetorquer 1 Init
    pinMode(MT_ENABLE, OUTPUT);
    pinMode(MT_IN1, OUTPUT);
    pinMode(MT_IN2, OUTPUT);
    pinMode(MT_ENABLE_LED, OUTPUT);
    pinMode(MT_IN1_LED, OUTPUT);
    pinMode(MT_IN2_LED, OUTPUT);

    digitalWrite(MT_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT_ENABLE_LED, HIGH);
    digitalWrite(MT_IN1, LOW);
    digitalWrite(MT_IN1_LED, LOW);
    digitalWrite(MT_IN2, LOW);
    digitalWrite(MT_IN2_LED, LOW);

    // Magnetorquer 2 Init
    pinMode(MT2_ENABLE, OUTPUT);
    pinMode(MT2_IN1, OUTPUT);
    pinMode(MT2_IN2, OUTPUT);
    pinMode(MT2_ENABLE_LED, OUTPUT);
    pinMode(MT2_IN1_LED, OUTPUT);
    pinMode(MT2_IN2_LED, OUTPUT);

    digitalWrite(MT2_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT2_ENABLE_LED, HIGH);
    digitalWrite(MT2_IN1, LOW);
    digitalWrite(MT2_IN1_LED, LOW);
    digitalWrite(MT2_IN2, LOW);
    digitalWrite(MT2_IN2_LED, LOW);

    // Magnetorquer 3 Init
    pinMode(MT3_ENABLE, OUTPUT);
    pinMode(MT3_IN1, OUTPUT);
    pinMode(MT3_IN2, OUTPUT);
    pinMode(MT3_ENABLE_LED, OUTPUT);
    pinMode(MT3_IN1_LED, OUTPUT);
    pinMode(MT3_IN2_LED, OUTPUT);

    digitalWrite(MT3_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT3_ENABLE_LED, HIGH);
    digitalWrite(MT3_IN1, LOW);
    digitalWrite(MT3_IN1_LED, LOW);
    digitalWrite(MT3_IN2, LOW);
    digitalWrite(MT3_IN2_LED, LOW);

    // IMU Init
    pinMode(IMU, OUTPUT);
    Serial.begin(9600);
    //115200
    Wire.begin();
    delay(2000);
    mpu.setup();
    delay(5000);

    // calibrate anytime you want to

    mpu.calibrateAccelGyro();
    mpu.calibrateMag();
    mpu.printCalibration();

    Setpoint = 0;
    magKp = 5.0;
    magKi = 0.01;
    magKd = 1;
    wheelKp = 5;
    wheelKi = 0.01;
    wheelKd = 1.0;
    
    wheelRollPID.SetMode(AUTOMATIC);
    wheelPitchPID.SetMode(AUTOMATIC);
    wheelYawPID.SetMode(AUTOMATIC);

    magRollPID.SetMode(AUTOMATIC);
    magPitchPID.SetMode(AUTOMATIC);
    magYawPID.SetMode(AUTOMATIC);

    prev_ms = millis();
}

void loop()
{
    int time = millis() - prev_ms;
    static uint32_t prev_ms = millis();
    if (time > 100) // Can be changed to increase print rate
    {
        mpu.update();
        takeAvgRoll[4] = mpu.getRoll();
        takeAvgYaw[4] = mpu.getYaw();
        takeAvgPitch[4] = mpu.getPitch();
        avg_roll = 0;
        avg_yaw = 0;
        avg_pitch = 0;
        for (int k = 0; k < 5; k++){
            avg_roll += takeAvgRoll[k];
            avg_yaw += takeAvgYaw[k];
            avg_pitch += takeAvgPitch[k];
        }

    roll_vel = avg_roll - prev_avg_roll;
    Serial.print("AVG roll - rad/s (x-forward (north)) : ");
    Serial.println(roll_vel);

    yaw_vel = avg_yaw - prev_avg_yaw;
    Serial.print("AVG yaw - rad/s (x-forward (north)) : ");
    Serial.println(yaw_vel);

    pitch_vel = avg_pitch - prev_avg_pitch;
    Serial.print("AVG pitch - rad/s (x-forward (north)) : ");
    Serial.println(pitch_vel);
    
    wheelRollPID.Compute();
    wheelPitchPID.Compute();
    wheelYawPID.Compute();

    magRollPID.Compute();
    magPitchPID.Compute();
    magYawPID.Compute();

    if(magRoll>DEADZONE){
        MT_set(magRoll,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (magRoll<-DEADZONE) {
        MT_set(magRoll,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }

    if(wheelRoll>DEADZONE){
        digitalWrite(RW_M1,HIGH);
        analogWrite(RW_E1, abs(wheelRoll)); //PWM Speed Control
    } else if (wheelRoll<-DEADZONE) {
        digitalWrite(RW_M1,LOW);
        analogWrite(RW_E1, abs(wheelRoll));
    } else {
        analogWrite(RW_E1,0);
    }

    if(magYaw>DEADZONE){
        MT_set2(magYaw,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (magYaw<-DEADZONE) {
        MT_set2(magYaw,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }

    if(wheelYaw>DEADZONE){
        digitalWrite(RW_M2,HIGH);
        analogWrite(RW_E2, abs(wheelYaw)); //PWM Speed Control
    } else if (wheelYaw<-DEADZONE) {
        digitalWrite(RW_M2,LOW);
        analogWrite(RW_E2, abs(wheelYaw));
    } else {
        analogWrite(RW_E2,0);
    }

    if(magPitch>DEADZONE){
        MT_set3(magPitch,MT_FWD);
        delay(MT_ON_TIME);
        MT_stop();
    } else if (magPitch<-DEADZONE) {
        MT_set3(magPitch,MT_REV);
        delay(MT_ON_TIME);
        MT_stop();
    }

    if(wheelPitch>DEADZONE){
        digitalWrite(RW_M3,HIGH);
        analogWrite(RW_E3, abs(wheelPitch)); //PWM Speed Control
    } else if (wheelPitch<-DEADZONE) {
        digitalWrite(RW_M3,LOW);
        analogWrite(RW_E3, abs(wheelPitch));
    } else {
        analogWrite(RW_E3,0);
    }
    
    prev_avg_roll = avg_roll;
    moveDown(takeAvgRoll);
    
    prev_avg_yaw = avg_yaw;
    moveDown(takeAvgYaw);
    
    prev_avg_pitch = avg_pitch;
    moveDown(takeAvgPitch);

    prev_roll_vel = roll_vel;
    prev_pitch_vel = pitch_vel;
    prev_yaw_vel = yaw_vel;
    
    prev_ms = millis();
    }
}

void moveDown(int takeAvg[5]){
    for (int i = 0; i < 4; i++){
        takeAvg[i] = takeAvg[i + 1];
    }
    return;
}

int MT_set(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT_IN1, magnitude);
            analogWrite(MT_IN1_LED, magnitude);
            digitalWrite(MT_IN2, LOW);
            digitalWrite(MT_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT_IN2, magnitude);
            analogWrite(MT_IN2_LED, magnitude);
            digitalWrite(MT_IN1, LOW);
            digitalWrite(MT_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

int MT_set2(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT2_IN1, magnitude);
            analogWrite(MT2_IN1_LED, magnitude);
            digitalWrite(MT2_IN2, LOW);
            digitalWrite(MT2_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT2_IN2, magnitude);
            analogWrite(MT2_IN2_LED, magnitude);
            digitalWrite(MT2_IN1, LOW);
            digitalWrite(MT2_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

int MT_set3(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT3_IN1, magnitude);
            analogWrite(MT3_IN1_LED, magnitude);
            digitalWrite(MT3_IN2, LOW);
            digitalWrite(MT3_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT3_IN2, magnitude);
            analogWrite(MT3_IN2_LED, magnitude);
            digitalWrite(MT3_IN1, LOW);
            digitalWrite(MT3_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

void MT_stop(){
    MT_set(0, MT_FWD);
}